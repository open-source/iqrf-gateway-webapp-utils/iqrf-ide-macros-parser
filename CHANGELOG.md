# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/).

## [Unreleased]

## [v1.0.1] - 2018-12.07
### Fixed:
- Fix PSR-4 autoloading

## [v1.0.0] - 2018-12-06
### Added:
- Add parsers for IQRF IDE Macro's file version 262146 and 262147.

[Unreleased]: https://github.com/iqrfsdk/iqrf-gateway-webapp-utils/iqrf-ide-macros-parser/compare/compare/v1.0.1...master
[v1.0.1]: https://gitlab.iqrf.org/open-source/iqrf-gateway-webapp-utils/iqrf-ide-macros-parser/compare/v1.0.0...v1.0.1
[v1.0.0]: https://gitlab.iqrf.org/open-source/iqrf-gateway-webapp-utils/iqrf-ide-macros-parser/compare/df32e5...v1.0.0
