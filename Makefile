# Copyright 2018 IQRF Tech s.r.o.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

CC_IGNORE=-i "coverage.*" -i "tests/temp/*" -i ".vscode/*" -i "build/api/*" -i "docs/api/*"

.PHONY: coverage cc fix-cc cs deps qa phpstan test

all: qa phpstan cc test

cache-purge:
	rm -rf temp/cache/

coverage: deps
	vendor/bin/tester -p phpdbg -c ./tests/php.ini --coverage ./coverage.html --coverage-src ./src ./tests

cc: temp/code-checker
	php temp/code-checker/code-checker -l --no-progress --strict-types $(CC_IGNORE)

fix-cc: temp/code-checker
	php temp/code-checker/code-checker -f -l --no-progress --strict-types $(CC_IGNORE)

cs: deps
	vendor/bin/codesniffer src tests

deps:
	composer install

qa: cs

phpstan: deps
	NETTE_TESTER_RUNNER=1 vendor/bin/phpstan analyse -c phpstan.neon

temp/code-checker:
	composer create-project nette/code-checker temp/code-checker --no-interaction

test: deps
	vendor/bin/tester -p phpdbg -c ./tests/php.ini ./tests
