# IQRF IDE Macros parser

[![Build Status](https://gitlab.iqrf.org/open-source/iqrf-gateway-webapp-utils/iqrf-ide-macros-parser/badges/master/pipeline.svg)](https://gitlab.iqrf.org/open-source/iqrf-gateway-webapp-utils/iqrf-ide-macros-parser/-/commits/master)
[![Test coverage](https://gitlab.iqrf.org/open-source/iqrf-gateway-webapp-utils/iqrf-ide-macros-parser/badges/master/coverage.svg)](https://gitlab.iqrf.org/open-source/iqrf-gateway-webapp-utils/iqrf-ide-macros-parser/-/commits/master)
[![Packagist Version](https://img.shields.io/packagist/v/iqrf/iqrf-ide-macros-parser)](https://packagist.org/packages/iqrf/iqrf-ide-macros-parser)
[![Packagist Downloads](https://img.shields.io/packagist/dm/iqrf/iqrf-ide-macros-parser)](https://packagist.org/packages/iqrf/iqrf-ide-macros-parser)
[![Apache License](https://img.shields.io/badge/license-APACHE2-blue.svg)](LICENSE)
[![API documentation](https://img.shields.io/badge/docs-api-brightgreen.svg)](https://apidocs.iqrf.org/iqrf-gateway-webapp-utils/iqrf-ide-macros-parser/)


PHP library for parsing IQRF IDE Macro files (`.iqrfmcr`) version `262146` and `262147`.

## Requirements

PHP 8.1 or higher.

## Installation

```shell script
composer require iqrf/iqrf-ide-macros-parser
```

## Nette DI configuration

```neon
extensions:
    iqrfMacros: Iqrf\IdeMacros\DI\IqrfMacrosExtension

iqrfMacros:
    path: path/to/macros.iqrfmcr
```

## License

This project is licensed under Apache License 2.0:

```
Copyright 2018 IQRF Tech s.r.o.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
