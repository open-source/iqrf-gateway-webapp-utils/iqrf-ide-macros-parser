<?php
/**
 * Copyright 2018 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\IdeMacros\DI;

use Iqrf\IdeMacros\MacroFileParser;
use Nette\DI\CompilerExtension;
use Nette\Schema\Expect;
use Nette\Schema\Schema;
use stdClass;

/**
 * Nette DI extension for IQRF IDE Macros parser
 * @property-read stdClass $config Configuration
 */
class IqrfMacrosExtension extends CompilerExtension {

	/**
	 * Returns the configuration schema
	 * @return Schema Configuration schema
	 */
	public function getConfigSchema(): Schema
	{
		return Expect::structure([
			'path' => Expect::string(),
		]);
	}

	/**
	 * Loads extension configuration
	 */
	public function loadConfiguration(): void {
		$builder = $this->getContainerBuilder();
		$builder->addDefinition($this->prefix('parser'))
			->setType(MacroFileParser::class)
			->setArguments([$this->config->path]);
	}

}
