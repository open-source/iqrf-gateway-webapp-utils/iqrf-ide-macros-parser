<?php
/**
 * Copyright 2018 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\IdeMacros;

/**
 * DPA packet
 */
class DpaPacket {

	/**
	 * @var int|null Network device address
	 */
	public ?int $nAdr = null;

	/**
	 * @var int|null Peripheral number
	 */
	public ?int $pNum = null;

	/**
	 * @var int|null Peripheral command
	 */
	public ?int $pCmd = null;

	/**
	 * @var int|null Hardware profile ID
	 */
	public ?int $hwpId = null;

	/**
	 * @var array<int> Binary buffer with DPA request data
	 */
	public array $pData = [];

	/**
	 * DPA packet object constructor
	 * @param string $packet DPA packet in string
	 * @param bool $bigEndian Is NADR and HWPID in big endian?
	 */
	public function __construct(string $packet, bool $bigEndian = false) {
		$packet = str_replace('.', '', $packet);
		if (strlen($packet) < 12) {
			return;
		}
		$array = [];
		foreach (str_split($packet, 2) as $value) {
			$array[] = hexdec($value);
		}
		if ($bigEndian) {
			$this->nAdr = ($array[0] << 8) | $array[1];
			$this->hwpId = ($array[4] << 8) | $array[5];
		} else {
			$this->nAdr = ($array[1] << 8) | $array[0];
			$this->hwpId = ($array[5] << 8) | $array[4];
		}
		$this->pNum = $array[2];
		$this->pCmd = $array[3];
		$this->pData = array_slice($array, 6);
	}

	/**
	 * Returns a DPA packet converted to a string
	 * @return string DPA packet converted to a string
	 */
	public function __toString(): string {
		if ($this->nAdr === null && $this->pNum === null &&
			$this->pCmd === null && $this->hwpId === null) {
			return '';
		}
		$packet = [
			($this->nAdr & 0xff),
			($this->nAdr >> 8),
			$this->pNum,
			$this->pCmd,
			($this->hwpId & 0xff),
			($this->hwpId >> 8),
			...$this->pData,
		];
		foreach ($packet as &$byte) {
			$byte = str_pad(dechex($byte), 2, '0', STR_PAD_LEFT);
		}
		return implode('.', $packet);
	}

}
