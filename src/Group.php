<?php
/**
 * Copyright 2018 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\IdeMacros;

/**
 * IQRF IDE Macro's group
 */
class Group {

	/**
	 * @var string Group name
	 */
	public string $name;

	/**
	 * @var int Group ID
	 */
	public int $id;

	/**
	 * @var bool Is group enabled?
	 */
	public bool $enabled;

	/**
	 * @var array<Macro> Array of macros
	 */
	public array $macros;

	/**
	 * Group object constructor
	 * @internal
	 * @param array<string> $array Macro group's data in an array
	 * @param MacroFileParser $file IQRF IDE Macro file
	 */
	public function __construct(array $array, MacroFileParser $file) {
		$this->name = $array[0];
		$this->enabled = $array[1] === 'True';
		$this->id = (int) $array[2];
		$macros = array_chunk(array_slice($array, 3), $file->getMacroSize());
		foreach ($macros as $macro) {
			$this->macros[] = new Macro($macro, $file);
		}
	}

	/**
	 * Returns the group in an array
	 * @return array{
	 *     id: int,
	 *     name: string,
	 *     enabled: bool,
	 *     macros: array<array{
	 *         name: string,
	 *         request: string,
	 *         note: string|null,
	 *         enabled: bool,
	 *         confirmation: bool,
	 *     }>
	 * } Group in an array
	 */
	public function toArray(): array {
		$array = [
			'name' => $this->name,
			'id' => $this->id,
			'enabled' => $this->enabled,
			'macros' => [],
		];
		foreach ($this->macros as $id => $macro) {
			$array['macros'][$id] = $macro->toArray();
		}
		return $array;
	}

}
