<?php
/**
 * Copyright 2018 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\IdeMacros;

use stdClass;

/**
 * IQRF IDE Macro
 */
class Macro extends stdClass {

	/**
	 * @var string Macro name
	 */
	public string $name;

	/**
	 * @var string Macro's request packet
	 */
	public string $request;

	/**
	 * @var string|null Macro's note
	 */
	public ?string $note = null;

	/**
	 * @var bool Is macro enabled?
	 */
	public bool $enabled;

	/**
	 * @var bool Macro's confirmation request
	 */
	public bool $confirmation;

	/**
	 * Macro object constructor
	 * @internal
	 * @param array<string> $array Macro's data in an array
	 * @param MacroFileParser $file IQRF IDE Macro file
	 */
	public function __construct(array $array, MacroFileParser $file) {
		$macroSize = $file->getMacroSize();
		$this->name = $array[0];
		$this->request = (string) (new DpaPacket($array[1], true));
		if ($file->version === 262147) {
			$this->note = $array[$macroSize - 4];
		}
		$this->enabled = $array[$macroSize - 3] === 'True';
		$this->confirmation = boolval($array[$macroSize - 2]);
	}

	/**
	 * Returns the macro in an array
	 * @return array{
	 *     name: string,
	 *     request: string,
	 *     note: string|null,
	 *     enabled: bool,
	 *     confirmation: bool,
	 * } Macro in an array
	 */
	public function toArray(): array {
		return [
			'name' => $this->name,
			'request' => $this->request,
			'note' => $this->note,
			'enabled' => $this->enabled,
			'confirmation' => $this->confirmation,
		];
	}

}
