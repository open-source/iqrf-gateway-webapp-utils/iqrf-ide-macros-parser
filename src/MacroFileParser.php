<?php
/**
 * Copyright 2018 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\IdeMacros;

/**
 * IQRF IDE Macro file parser
 */
class MacroFileParser {

	/**
	 * @var int IQRF IDE Macro's file version
	 */
	public int $version;

	/**
	 * @var string IQRF IDE Macro's file description
	 */
	public string $description;

	/**
	 * @var int IQRF IDE Macro's file type
	 */
	public int $type;

	/**
	 * @var array<Group> IQRF IDE Macro's groups
	 */
	public array $groups;

	/**
	 * Constructor
	 * @param string $fileName IQRF IDE Macro's file name
	 */
	public function __construct(
		private readonly string $fileName,
	) {
	}

	/**
	 * Read IQRF IDE Macro's file and parse it
	 * @return array<Group> Parsed macros
	 */
	public function read(): array {
		$fileContent = parse_ini_file($this->fileName, true)['Macro'];
		$this->version = (int) $fileContent['Version'];
		$this->description = $fileContent['Description'];
		$this->type = (int) $fileContent['Type'];
		return $this->parse($fileContent);
	}

	/**
	 * Returns IQRF IDE Macro's size
	 * @return int IQRF IDE Macro's size
	 */
	public function getMacroSize(): int {
		return $this->version === 262147 ? 6 : 5;
	}

	/**
	 * Convert objects into arrays
	 * @param array<Group> $macros IQRF IDE Macros in an array of objects
	 * @return array<array{
	 *      id: int,
	 *      name: string,
	 *      enabled: bool,
	 *      macros: array<array{
	 *          name: string,
	 *          request: string,
	 *          note: string|null,
	 *          enabled: bool,
	 *          confirmation: bool,
	 *      }>
	 *  }> IQRF IDE Macros in an array
	 */
	public function toArray(array $macros): array {
		return array_map(static fn (Group $group) => $group->toArray(), $macros);
	}

	/**
	 * Parse IQRF IDE Macros into objects
	 * @param array<string, int|string> $fileContent IQRF IDE Macro's file content
	 * @return array<Group> Array of objects with IQRF IDE macros
	 */
	private function parse(array $fileContent): array {
		$ascii = Hex::toAscii($fileContent['Macros']);
		$array = array_slice(explode("\r\n", trim($ascii)), 3);
		$chunks = array_chunk($array, 12 * $this->getMacroSize() + 3);
		$groups = [];
		foreach ($chunks as $chunk) {
			$groups[] = new Group($chunk, $this);
		}
		return $groups;
	}

}
