<?php

/**
 * TEST: Iqrf\IdeMacros\Hex
 * @covers Iqrf\IdeMacros\Hex
 * @phpVersion >= 7.1
 * @testCase
 */
declare(strict_types = 1);

namespace Test\Iqrf\IdeMacros;

use Iqrf\IdeMacros\Hex;
use Nette\Utils\FileSystem;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/bootstrap.php';

/**
 * Tests for HEX utils
 */
class HexTest extends TestCase {

	/**
	 * @var string Path to a directory with files for testing
	 */
	private string $path = __DIR__ . '/data/';

	/**
	 * Test function to convert HEX to ASCII
	 */
	public function testToAscii(): void {
		$hex = FileSystem::read($this->path . 'macros_180517.hex');
		$expected = FileSystem::read($this->path . 'macros_180517.ascii');
		Assert::equal($expected, Hex::toAscii($hex));
	}

}

$test = new HexTest();
$test->run();
