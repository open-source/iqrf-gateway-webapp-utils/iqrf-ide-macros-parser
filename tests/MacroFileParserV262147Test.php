<?php

/**
 * TEST: Iqrf\IdeMacros\MacroFileParser
 * @covers Iqrf\IdeMacros\MacroFileParser
 * @phpVersion >= 7.1
 * @testCase
 */
declare(strict_types = 1);

namespace Test\Iqrf\IdeMacros;

use Iqrf\IdeMacros\MacroFileParser;
use Nette\Utils\FileSystem;
use Nette\Utils\Json;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/bootstrap.php';

/**
 * Tests for IQRF IDE Macros parser
 */
class MacroFileParserV262147Test extends TestCase {

	/**
	 * @var MacroFileParser IQRF IDE Macros parser
	 */
	private MacroFileParser $parser;

	/**
	 * @var string Path to a directory with files for testing
	 */
	private string $path = __DIR__ . '/data/';

	/**
	 * Test function for IQRF IDE Macros file reading and parsing
	 */
	public function testRead(): void {
		$expected = Json::decode(FileSystem::read($this->path . 'macros_181017.json'), forceArrays: true);
		$macros = $this->parser->read();
		Assert::equal($expected, $this->parser->toArray($macros));
	}

	/**
	 * Set up the test environment
	 */
	protected function setUp(): void {
		$this->parser = new MacroFileParser($this->path . 'macros_181017.iqrfmcr');
	}

}

$test = new MacroFileParserV262147Test();
$test->run();
